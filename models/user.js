var mongoose = require('mongoose');

// Define our beer schema
var UserSchema   = new mongoose.Schema({
  // _id:String,
  name: {type:String, default:""},
  email: {type:String, default:""},
  pendingTasks: {type:[String], default:[]},
  dateCreated: {type:Date, default: Date()}
});

module.exports = mongoose.model('User', UserSchema);