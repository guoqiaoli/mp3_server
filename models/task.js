var mongoose = require('mongoose');

var TaskSchema = new mongoose.Schema({
	// _id:String,
	name: {type:String, default:""},
	description: {type: String, default:"No Current Description"},
	deadline: {type:Date, default: Date()},
	completed: {type:Boolean, default:false},
	assignedUser: {type: String, default:""},
	assignedUserName: {type: String, default:"unassigned"},
	dateCreated: {type:Date, default: Date()}
});
module.exports = mongoose.model('Task', TaskSchema);