// Load required packages
var mongoose = require('mongoose');

// Define our beer schema
var UserSchema   = new mongoose.Schema({
  name: String,
  email: Number,
  pendingTasks: [String],
  dateCreated: Date
});

var TaskSchema = new mongoose.Schema({
	name: String,
	email: String,
	deadline: Date,
	completed: Boolean,
	assignedUser: [String],
	assignedUserName: String,
	dataCreated: Date
});

// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);
module.exports = mongoose.model('Task', TaskSchema);