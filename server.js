// Get the packages we need
var express = require('express');
var mongoose = require('mongoose');
// var Llama = require('./models/llama');
var User = require('./models/user');
var Task = require('./models/task')
var bodyParser = require('body-parser');
var url = require('url');
var router = express.Router();
var MyObjectId = mongoose.Types.ObjectId;

//replace this with your Mongolab URL
mongoose.connect('mongodb://guoqiaoli:lgq19920616@ds029338.mongolab.com:29338/guoqiaoli');

// Create our Express application
var app = express();

// Use environment defined port or 4000
var port = process.env.PORT || 4000;

//Allow CORS so that backend and frontend could pe put on different servers
var allowCrossDomain = function(req, res, next) {
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept");
  next();
};
app.use(allowCrossDomain);

// Use the body-parser package in our application
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// All our routes will start with /api
app.use('/api', router);

//Default route here
var homeRoute = router.route('/');

homeRoute.get(function(req, res) {
  // console.log("abcde");
  res.json({ message: 'Hello World!' });
});

homeRoute.post(function(req, res){
	var user = new Llama();
	user.name = req.body.name;
	user.height = req.body.height;

	res.send(req.body.name);
});
//Llama route 
router.route("/users")
	.get(function(req, res) {
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;

		var errmsg="";
		var msg;

		// var q = JSON.parse(query['where'])
		var q={};
		var select={};
		var sort={};
		if(query['where']) {
			q=JSON.parse(query['where']);}
		// 	if(q._id)
		// 		if(MyObjectId(q._id))
		// 			q._id = MyObjectId(q._id);
		// }

		if(query['select']) select=JSON.parse(query['select']);
		if(query['sort']) sort=JSON.parse(query['sort']);
		User.find(q,"pendingTasks name email dateCreated", {skip:query['skip'], limit:query['limit']},function(err, data){
			if(err){
				errmsg = err.name;
				msg = {message:errmsg, data:""};
				res.send(msg);
			}
			else{
				res.status(200);
			if(data.length==0){
				errmsg = "404 Users Not Found";
				msg = {message:errmsg, data:data};
				res.send(msg);
			}
			else{
			if(query['count']){
				errmsg = "User Found";
				msg = {message:errmsg, data: count};
				res.send(msg);
				var count=0;
				for(obj in data)
					count++;
				res.send(msg);
			}
			else{
				errmsg = "User Found";
				msg = {message:errmsg, data:data};
				res.send(msg);
			}}}
		}).sort(sort).select(select);
	})

	.post(function(req, res){
		// console.log("post goes here");
		var user = new User();
		var errmsg = "User created";
		var resMsg;
		// console.log("name: "+ req.body.name);
		user.name = req.body.name;
		user.email = req.body.email;
		user.pendingTasks = req.body.pendingTasks;
		user.dateCreated = req.body.dateCreated;

		if(!req.body.name || !req.body.email){
			errmsg = "No name or email provided.";
			resMsg = {message: errmsg, data: user};
			res.send(resMsg);
		}
		//find if email exist
		else{
		// if(req.body.email.indexOf("@")<0 || req.body.email.indexOf(".")<0){
		// 	errmsg = "This is not a valid email";
		// 	resMsg = {message: errmsg, data: user};
		// 	res.send(resMsg);
		// }
		// else{
		User.findOne({email:req.body.email},"pendingTasks name email dateCreated",function(err,data){
			if(err){
			}
			else{
				if(data){
					errmsg="Email already exist";
					resMsg = {message: errmsg, data: user};
					res.send(resMsg);
				}
				else{
					user.save(function(err){
				if(err){
					errmsg=err.name;
					resMsg = {message: errmsg, data: user};
					res.send*(resMsg);
				}
				else{
					res.status(201);
					errmsg="User created";
					resMsg = {message: errmsg, data: user};
					res.send(resMsg);
				}
			});
				}
			}
		});
		}
	})

	.options(function(req, res){
		 res.writeHead(200);
      res.end();
	});
router.route('/users/:user_id')
	.get(function(req, res) {
		User.find({_id:req.params.user_id},function(err,data){
			if(err){
				errmsg=err.name;
				msg={message:errmsg, data:""};
				res.send(msg);
			}
			else{
			if(data.length==0){
				res.status(404);
				errmsg = "404 Users Not Found";
				msg = {message:errmsg, data:data};
				res.send(msg);
			}
			else{
			res.status(200);
			errmsg="User Founded";
			msg={message:errmsg, data:data};
			res.send(msg);}}
		});
	})

	.put(function(req, res){
		var errmsg;
		var msg;
		var update={};
		User.findOne({_id:req.params.user_id},function(err,data){
			if(err){
				errmsg=err.name;
				msg={message:errmsg, data:""};
				res.send(msg);
			}
			else{
			if(data.length==0){
				res.status(404);
				errmsg="404 User Not Found";
				msg={message:errmsg, data:""};
				res.send(msg);
			}
			else{
			var flag = false;
			if(req.body.name) data.name=req.body.name;
			if(req.body.email) data.email=req.body.email;
			if(req.body.pendingTask){
					for(task in data.pendingTasks){
						//if exist, remove
						if(data.pendingTasks[task]==req.body.pendingTask){
							flag=true;
							data.pendingTasks.splice(task,1);
						}
					}
					//if not exist, push
					if(!flag){
						data.pendingTasks.push(req.body.pendingTask);
					}
			}
			res.status(200);
			errmsg="Updated"
			msg={message:errmsg, data:data};
			res.send(msg);
			data.save();}}
		})
	})

	.delete(function(req, res){
		var errmsg;
		var msg;
		User.remove({ _id: req.params.user_id }, function(err,data) {
    		if (!err) {
    			if(data==0){
    				res.status(404);
    				errmsg = "404 User Not Found";
    				msg={message:errmsg, data:data};
					res.send(msg);
    			}
    			else{
    			res.status(200);
    			errmsg = "User Removed";
    			msg={message:errmsg, data:data};
				res.send(msg);
				}
    		}
    		else {

				errmsg=err.name;
				msg={message:errmsg, data:""};
				res.send(msg);
    		}
});
	});

router.route('/tasks')
	.get(function(req, res){
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
		// console.log(query['where']);
		var errmsg="";
		var msg;

		var q={};
		var select={};
		var sort={};
		if(query['where']) {
			q=JSON.parse(query['where']);
			// if(q._id)
			// 	if(MyObjectId(q._id))
			// 			q._id = MyObjectId(q._id);
		}
		if(query['select']) select=JSON.parse(query['select']);
		if(query['sort']) sort=JSON.parse(query['sort']);
		Task.find(q,"name deadline description assignedUser assignedUserName completed dateCreated", {skip:query['skip'], limit:query['limit']},function(err, data){
			if(err){
				errmsg = err.name;
				msg = {message:errmsg, data:""};
				res.send(msg);
			}
			else{
			if(data.length==0){
				errmsg = "404 Task Not Found";
				msg = {message:errmsg, data:data};
				res.send(msg);
			}
			else{
			if(query['count']){
		
				var count=0;
				for(obj in data)
					count++;
				errmsg = "Task Found";
				msg = {message:errmsg, data:count};
				res.send(msg);
			}
			else{
				errmsg = "Task Found";
				msg = {message:errmsg, data:data};
				res.send(msg);
			}}
		}
		}).sort(sort).select(select);
	})

	.post(function(req, res){
		var task = new Task();
		var errmsg = "Task Created";
		var msg;
		task.name = req.body.name;
		task.deadline = req.body.deadline;
		task.assignedUserName = req.body.assignedUserName;
		task.completed = req.body.completed;
		task.assignedUser = req.body.assignedUser;
		task.dateCreated = req.body.dateCreated;

		if(!req.body.name || !req.body.deadline){
			errmsg = "No name or deadline provided.";
			msg = {message: errmsg, data: task};
			res.send(msg);
		}

		var msg;
		if(errmsg == "Task Created"){
			task.save(function(err,data){
				if(err){

					errmsg=err.name;
					msg = {message: errmsg, data: data};
					res.send(msg);
				}
				else{
					msg = {message: errmsg, data: task};
					res.send(msg);
				}
			});
		}
	})

	.options(function(req, res){
		 res.writeHead(200);
      res.end();
	});

router.route('/tasks/:task_id')
	.get(function(req, res) {
		var task_id = req.params.task_id;
		var errmsg = "";
		var msg;
		console.log(task_id);
		Task.find({_id:task_id},"name deadline description assignedUser assignedUserName completed",function(err, data){
			if(err){
				errmsg = err.name;
				msg = {message: errmsg, data:""};
				res.send(msg);
			}
			else{
			if(data.length==0){
				res.status(404);
				errmsg = "404 Task Not Found";
				msg = {message:errmsg, data:data};
				res.send(msg);
			}
			else{
				res.status(200);
			errmsg = "Task Found";
			msg = {message: errmsg, data:data};
			res.send(msg);}}
		});
		// res.send(msg);
	})

	.put(function(req, res){
		var errmsg;
		var msg;
		Task.findOne({_id:req.params.task_id},function(err,data){
			if(err){
			
				errmsg=err.name;
				msg={message:errmsg,data:""};
				res.send(msg);
			}
			else{
			if(data.length==0){
				res.status(404);
				errmsg = "404 Task Not Found";
				msg = {message:errmsg, data:data};
				res.send(msg);
			}
			else{
				res.status(200);
			if(req.body.name) data.name=req.body.name;
			if(req.body.deadline) data.email=req.body.deadline;
			if(req.body.description) data.description=req.body.description;
			if(req.body.assignedUser) data.assignedUser=req.body.assignedUser;
			if(req.body.assignedUserName) data.assignedUserName=req.body.assignedUserName;
			if(req.body.completed) data.completed=req.body.completed;
			data.save();
			errmsg="Task Updated";
			msg={message:errmsg,data:data};
			res.send(msg);}}
		})
	})

	.delete(function(req, res){
		var errmsg;
		var msg;
		Task.remove({_id:req.params.task_id},function(err,data){
			if(err){
	
				errmsg=err.name;
				msg={message:errmsg,data:""};
				res.send(msg);
			}
			else{
				if(data==0){
					res.status(404);
				errmsg="404 Task Not Found";
				msg={message:errmsg,data:data};
				res.send(msg);
				}
				else{
				res.status(200);
				errmsg="Task Deleted";
				msg={message:errmsg,data:data};
				res.send(msg);
			}
			}
		});
	});
//Add more routes here

// Start the server
app.listen(port);
console.log('Server running on port ' + port); 